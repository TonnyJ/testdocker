<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Database\Context;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var Context @inject */
    public $database;
    
    public function actionDefault() {
        $users = $this->database->table('users');
        bdump($users->fetchAll());
        $this->template->users = $users;
    }
}
